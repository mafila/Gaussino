# Generation
Configuration related to the generation phase.

## Main Generation Configuration

```{eval-rst}
.. currentmodule:: Gaussino.Generation

.. autoclass:: GaussinoGeneration
   :show-inheritance:
   :undoc-members:
   :special-members: __apply_configuration__
   :members:
   :private-members:
```
